
Drupal.behaviors.safariSearch = function(context) {
  $('#search-form #edit-keys:not(.safarisearch-processed), input:text[id^="edit-search-"]:not(.safarisearch-processed)', context).each(function() {
    var input = $(this).addClass('safarisearch-processed');
    if (jQuery.browser.safari) {
      // jQuery 1.2 does no longer allow changing the type property.
      input.get(0).type = 'search';
      input.attr({
        'size': 18,
        'placeholder': $('input:submit', input[0].form).val()
      });
    }
    else {
      if (input.get(0).id != 'edit-keys') {
        input.attr('size', 11);
      }

      var clear = $('<span class="safarisearch-r"></span>').click(function() {
        input.val('').get(0).focus();
        update.apply(input.get(0));
      });

      var update = function() {
        clear[this.value != '' ? 'addClass' : 'removeClass']('safarisearch-dirty');
      };

      input.before('<span class="safarisearch-l"></span>').after(clear).wrap('<span class="safarisearch"></span>').keyup(update);
      update.apply(input.get(0));
    }
  });
};
